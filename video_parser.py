# imports
import cv2
import os

import pprint
pp = pprint.PrettyPrinter()

def extract_annotate_frame(input_video_path, output_frame_dir, app_timestamps_info, filename):
    # print(input_video_path, output_frame_dir, app_timestamps_info, filename)
    i = 0
    j = len(app_timestamps_info)
    print('reading frames of the video file ...')
    cap = cv2.VideoCapture(input_video_path)
    fps = cap.get(cv2.CAP_PROP_FPS)
    # print('fps', fps)
    timestamps = []
    # snap number
    n = 1
    while(cap.isOpened() and i < j):
        frame_exists, curr_frame = cap.read()
        if frame_exists:
            timestamp = cap.get(cv2.CAP_PROP_POS_MSEC)
            timestamps.append(timestamp)
            abs_diff = abs(int(timestamp) - app_timestamps_info[i]['timestamp_in_ms'])
            # if(int(timestamp) == app_timestamps_info[i]['timestamp_in_ms'] or abs_diff < fps):
            if(app_timestamps_info[i]['timestamp_in_ms'] + n * 1000 <=  int(timestamp) <=  app_timestamps_info[i]['timestamp_in_ms'] + 11000):
                # write the frame
                p1 = filename.split('.')[0]
                p2, p3 = app_timestamps_info[i]['timestamp'].split(':')
                ext = '.jpg'
                # output_frame_filename = p1 + '-' + p2 + '-' + p3 + ext
                output_frame_filename = p1 + '-' + p2 + '-' + p3 + '-snap' + str(n) + ext
                output_frame_path = os.path.join(output_frame_dir, output_frame_filename)
                print('frame extracted at timestamp {min}:{sec} snap{snapnum}...'.format(min=p2, sec=p3, snapnum=str(n)))
                cv2.imwrite(output_frame_path, curr_frame)

                # increment n for next snap
                n = n + 1
                if(n != 11):
                    continue
                # reinitialize snap number
                n = 1
                # move to the next given timestamp
                i = i + 1
                # chekck if there is no more given timestamp
                if(i >= j):
                    break
                # check if the timestamp is same as the previous one
                while(app_timestamps_info[i]['timestamp_in_ms'] == app_timestamps_info[i-1]['timestamp_in_ms']):
                    i = i + 1
                    if(i >= j):
                        break
        else:
            break

    cap.release()
    # print(app_timestamps_info)
    # print(timestamps)
    return
