# Video Frame Extractor

The script extracts frames from the Uni Zurich's DP video dataset according to the parsed timestamps from the classification data

## Prerequisites:
- Python 3.7+
- opencv-python 
  - Installation: pip install opencv-python
- openpyxl
  - Installation: pip install openpyxl

## Dataset download/addition:
- download the Videos.zip from the location: https://figshare.com/s/048c984854a59429d0f0
- For Linda: places all collected videos in a folder labeled 'Videos' at the root of the 'uzh-dp-video-frame-extractor' folder.

## How to run:
- unzip the folder 'Videos.zip'
- change your working directory to 'video_frame_extractor'
- copy the folder 'Videos'(including the subfolders) into the 'uzh-dp-video-frame-extractor' folder
- now the folder structure should look like the following:
```bash
├── video_frame_extractor
│   ├── README.md
│   ├── classification_data.xlsx
│   ├── main.py
│   ├── utils.py
│   ├── video_parser.py
│   ├── xlsx_parser.py
│   ├── Videos
│   │   ├── entertainment
│   │   │   ├── entertainment_##-**-**.mp4
│   │   │   ├── entertainment_##-**-**.mp4
│   │   ├── family
│   │   │   ├── family_##-**-**.mp4
│   │   │   ├── family_##-**-**.mp4
│   │   │   ... ... ...
│   │   │   ... ... ...
```
- in the command line: run `python main.py`
- this will invoke the script to parse the classification_data.xlsx & extract frames from the video files in 'Videos' folder
- once the frame extraction is completed, all the extracted frames will be available under the folder 'images'
- now the folder structure should look like the following:
```bash
├── video_frame_extractor
│   ├── README.md
│   ├── classification_data.xlsx
│   ├── main.py
│   ├── utils.py
│   ├── video_parser.py
│   ├── xlsx_parser.py
│   ├── Videos
│   │   ├── entertainment
│   │   │   ├── entertainment_##-**-**.mp4
│   │   │   ├── entertainment_##-**-**.mp4
│   │   ├── family
│   │   │   ├── family_##-**-**.mp4
│   │   │   ├── family_##-**-**.mp4
│   │   │   ... ... ...
│   │   │   ... ... ...
│   ├── images
│   │   ├── entertainment
│   │   │   ├── ...
│   │   │   ├── ...
│   │   ├── family
│   │   │   ├── ...
│   │   │   ├── ...
│   │   │   ... ... ...
│   │   │   ... ... ...
```

## Example Script Output

```bash
Vespoli:UZH-DP-Video-Frame-Extractor KevinMoran$ python main.py
######################## parsing xlsx file ############################
Photography worksheet parsed
--------------------------------------
Family worksheet parsed
--------------------------------------
MusicAndAudio worksheet parsed
--------------------------------------
Entertainment worksheet parsed
--------------------------------------
Shopping worksheet parsed
--------------------------------------
NewsAndMagazines worksheet parsed
--------------------------------------
Social worksheet parsed
--------------------------------------
Communication worksheet parsed
--------------------------------------
######################## parsing video files ##########################
---------------------------------------------------------------
category: music
app_name: Youtube-Music, app_id: 2
video filename: music_2-Youtube-Music.mp4
reading frames of the video file ...
frame extracted at timestamp 0:4 ...
frame extracted at timestamp 0:8 ...
frame extracted at timestamp 1:6 ...
frame extracted at timestamp 1:24 ...
frame extracted at timestamp 1:32 ...
frame extracted at timestamp 3:14 ...
frame extracted at timestamp 3:57 ...
frame extracted at timestamp 5:42 ...
frames successfully extracted
```

## Video file naming convention:
The script expects the filename of a video in the following convention:
- category_appid-this-is-app-name.mp4

Category can be one of the following:
- photography/family/music/entertainment/shopping/news/social/communication
