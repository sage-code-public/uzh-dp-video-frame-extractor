# imports
from openpyxl import load_workbook
import pprint
pp = pprint.PrettyPrinter()

# util functions
def get_category_folder_name(category):
    if(category == 'Photography'):
        return 'photography'
    elif(category == 'Family'):
        return 'family'
    elif(category == 'MusicAndAudio'):
        return 'music'
    elif(category == 'Entertainment'):
        return 'entertainment'
    elif(category == 'Shopping'):
        return 'shopping'
    elif(category == 'NewsAndMagazines'):
        return 'news'
    elif(category == 'Social'):
        return 'social'
    elif(category == 'Communication'):
        return 'communication'

def get_xlsx_parsed_info(file):
    # loading the xlsx file
    wb = load_workbook(file, data_only=True)

    # parsing category names from worksheet names
    worksheet_names = wb.sheetnames
    category_names = worksheet_names[4:len(worksheet_names)]

    # 'Photography', 'Family', 'Entertainment' used due to the
    # data inconsistency in other worksheets
    # category_names = ['Photography', 'Family', 'Entertainment']
    # category_names = ['Family']

    # parsing info for every category
    parsed_info = {}
    for category in category_names:
        category_folder_name = get_category_folder_name(category)
        parsed_info[category_folder_name] = {}
        worksheet = wb[category]
        # parsing info for every app in the category
        start = 39
        end = 1171
        step = 39
        for index, start_row in enumerate(range(start,end,step)):
            app_id = index + 1
            app_name_cell_id = 'B' + str(app_id + 1)
            app_name = worksheet[app_name_cell_id].value
            parsed_info[category_folder_name][str(app_id)] = {'app_name' : app_name, 'timestamps_info': []}

            # print('app_id', app_id)
            # print('app_name', app_name)

            while(True):
                # parsing timestamp
                timestamp_cell_value = worksheet['A' + str(start_row)].value
                # print(timestamp_cell_value)
                if(timestamp_cell_value == None):
                    break
                min = timestamp_cell_value.hour
                sec = timestamp_cell_value.minute
                # populating dictionary with timestamp details
                timestamp_info = {}
                timestamp_info['timestamp'] = str(min) + ':' + str(sec)
                timestamp_info['timestamp_in_ms'] = (min * 60 * 1000) + (sec * 1000)


                # print category & app id if a cell has timestamp vlaue lower than the vlaue of upper cell
                if(len(parsed_info[category_folder_name][str(app_id)]['timestamps_info']) > 0):
                    if(timestamp_info['timestamp_in_ms'] < parsed_info[category_folder_name][str(app_id)]['timestamps_info'][-1]['timestamp_in_ms']):
                        print(category_folder_name, app_id)

                timestamp_info['dp_info'] = {
                                            'NG' : worksheet['M' + str(start_row)].value
                                            , 'OBST' : worksheet['N' + str(start_row)].value
                                            , 'RM' : worksheet['O' + str(start_row)].value
                                            , 'PCP' : worksheet['P' + str(start_row)].value
                                            , 'IntCur' : worksheet['Q' + str(start_row)].value
                                            , 'SNE' : worksheet['R' + str(start_row)].value
                                            , 'HC' : worksheet['S' + str(start_row)].value
                                            , 'SIB' : worksheet['T' + str(start_row)].value
                                            , 'BAS' : worksheet['U' + str(start_row)].value
                                            , 'II' : worksheet['V' + str(start_row)].value
                                            , 'HI' : worksheet['W' + str(start_row)].value
                                            , 'PRES' : worksheet['X' + str(start_row)].value
                                            , 'AM' : worksheet['Y' + str(start_row)].value
                                            , 'TWE' : worksheet['Z' + str(start_row)].value
                                            , 'FH' : worksheet['AA' + str(start_row)].value
                                            , 'DA' : worksheet['AB' + str(start_row)].value
                                            , 'TQ' : worksheet['AC' + str(start_row)].value
                                            , 'FA' : worksheet['AD' + str(start_row)].value
                                            , 'PZ' : worksheet['AE' + str(start_row)].value
                                            , 'SP' : worksheet['AF' + str(start_row)].value
                                            }
                parsed_info[category_folder_name][str(app_id)]['timestamps_info'].append(timestamp_info)
                start_row += 1
        print('{category} worksheet parsed'.format(category=category))
        print('--------------------------------------')
    # pp.pprint(parsed_info)
    return parsed_info
