# imports
import os
import subprocess
from xlsx_parser import get_xlsx_parsed_info
from video_parser import extract_annotate_frame
from utils import *

import pprint
pp = pprint.PrettyPrinter()

import warnings
warnings.filterwarnings("ignore")

import datetime

# paths
xlsx_path = 'classification_data.xlsx'
input_root_dir = 'Videos'
output_root_dir = 'images'
tmp_dir = 'tmp'

begin_time = datetime.datetime.now()

try:
    # parsing xlsx file
    print('######################## parsing xlsx file ############################')
    parsed_info = get_xlsx_parsed_info(xlsx_path)
    # pp.pprint(parsed_info)
    write_json_file(parsed_info)
except Exception as e:
    print('Error: There was an error while parsing the xlsx file')
    print(e)
else:
    print('######################## parsing video files ##########################')
    if(not os.path.exists(input_root_dir)):
        print('Error: directory \'Videos\' not found. please make sure the root directory for the videos has the name \'Videos\'')
    # r = root, d = directories, f = files
    for r, d, f in os.walk(input_root_dir):
        # print('r', r)
        # print('d', d)
        # print('f', f)
        for filename in f:
            name, ext = os.path.splitext(filename)
            # if(ext == '.mp4'):
            if(ext in ['.mp4', '.flv', '.mkv']):
                print('---------------------------------------------------------------')
                try:
                    # parse category/app_id/app_name
                    filename_parts1 = filename.split('_')
                    category = filename_parts1[0]
                    filename_parts2 = filename_parts1[1].split('-')
                    app_id = filename_parts2[0]
                    app_name = '-'.join(filename_parts2[1:]).split('.')[0]

                    # print category/app_id/app_name
                    print('category: {category}'.format(category=category))
                    print('app_name: {app_name}, app_id: {app_id}'.format(app_name=app_name, app_id=app_id))
                    print('video filename: {filename}'.format(filename=filename))
                except Exception as e:
                    print('Error: there was error while parsing category/app_id/app_name from the filename: ', filename)
                    print(e)
                else:
                    try:
                        # get app timestamps from the parsed_info
                        app_timestamps_info = parsed_info[category][app_id]['timestamps_info']
                    except Exception as e:
                        print('KeyError: no such key found in the parsed xlsx data dictionary')
                        print(e)
                    else:
                        try:
                            # create dirs/sub dirs
                            if(not os.path.exists(output_root_dir)):
                                os.mkdir(output_root_dir)
                            category_dir = os.path.join(output_root_dir, category)
                            if(not os.path.exists(category_dir)):
                                os.mkdir(category_dir)
                            app_dir = app_id + '-' + app_name
                            output_frame_dir = os.path.join(category_dir, app_dir)
                            if(not os.path.exists(output_frame_dir)):
                                os.mkdir(output_frame_dir)
                        except Exception as e:
                            print('Error: there was an error while creating output directories')
                            print(e)
                        else:
                            # extract frames
                            if(len(app_timestamps_info) != 0):
                                input_video_path = os.path.join(input_root_dir, category, filename)
                                try:
                                    if(not os.path.exists(tmp_dir)):
                                        os.mkdir(tmp_dir)
                                    no_sound_video_path = os.path.join(tmp_dir, filename)
                                    # convert original video -> no sound video & copy to tmp
                                    command = ["ffmpeg", "-i", input_video_path, "-c", "copy", "-an", no_sound_video_path]
                                    subprocess.run(command, stdout=subprocess. DEVNULL, stderr=subprocess. DEVNULL)
                                    extract_annotate_frame(no_sound_video_path, output_frame_dir, app_timestamps_info, filename)
                                    # extract_annotate_frame(input_video_path, output_frame_dir, app_timestamps_info, filename)
                                    # print('frames successfully extracted')
                                except Exception as e:
                                    # print('Error: there was an error while extracting frames from the video file: ', filename)
                                    print(e)
                                finally:
                                    try:
                                        os.remove(no_sound_video_path)
                                    except Exception as e:
                                        print(e)
                            else:
                                print('no timestamps provided in the parsed xlsx data')
print('total execution time: ', datetime.datetime.now() - begin_time)
