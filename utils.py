import json

def write_json_file(dict):
    with open('parsed_xlsx_info.json', 'w') as fp:
        json.dump(dict, fp)
